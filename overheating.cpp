
#include "overheating.h"
int main() {
    std::string strtemp;
    long double Ctm=0;
    int m = menu();
    int flag;
     while (m != 4) {
            switch (m) {
            case 1: {                
                strtemp = inputfile();
                break;
            }
            case 2: {
                std::cout << "Input temperature\n";
                std::cin >> Ctm;
                break;
            }
            case 3: {
                long double res = test(strtemp, Ctm);
                break;
            }
            case 4: {
                exit(0);
                break;
            }
            }
            m = menu();
        }

    return 0;
}
//изменение момента двигателя в зависимости от скорости коленвала.
long double checkv(const long double Vk, const int V[], const int M[], const  int size) {
     long double m=0;
    for (int i1 = 0; i1 < size-1;i1++) {
        if ((Vk >= V[i1]) && (Vk < V[i1 + 1]))
        {   
            m = (((long double)M[i1 + 1] - M[i1]) * (Vk - (long double)V[i1]))/((long double)V[i1+1]-V[i1]) + M[i1];
            break;
        }
    }
    //вариант поточенчного изменения
    //if (Vk > V[1]) { i++; }//>
    //if (Vk > V[2]) { i++; }//>
    //if (Vk > V[3]) { i++; }//>
    //if (Vk > V[4]) { i++; }//>250
    //if (Vk > V[5]) { i++; }//>300
    //std::cout << "M= " << m << " m[i] = " << M[i] << "\n";
    return m;
}
//ввод конфигурационного файла в случае некорректного ввода используется стандартные настройки из файла data.txt
//легко можно либо зациклить программу до ввода корректного файла с настройками, или прекращать программу при отсутствии файла.
std::string inputfile() {
    std::string stmp;
    std::cin >> stmp;
    std::ifstream infile;
    int flag = -1;
    infile.open(stmp);
    while ((flag != 0)&&(!infile.is_open())) {
        if (!infile.is_open()) {
            char ch;
            std::cout << "No such file or directory, load standard configuration\n";
            stmp = "data.txt";
            infile.open(stmp);
            std::cout << "continue or return to previous step?(c/r)\n";
            std::cin >> ch;
            if (ch == 'c') { flag = 0; break; }
            else { flag = 1; break; }// (ch == 'r') установка только r как выбора действий не очень удобна.
            
        }
    }
    infile.close();
    return stmp;

}

int menu() {
    std::ifstream infile;
    std::stringstream sstream;
    std::string strtemp;
    int men=-1,flag=0;
    std::cout << "Choose menu:\n";
    std::cout << "1) Specify name of configuration file.\n";
    std::cout << "2) Enter temperature \n";
    std::cout << "3) Start test \n";
    std::cout << "4) Exit \n";
    std::cin >> men;
    return men;
}
//Собственно сам тест с расчётом времени работы до перегрева двигателя.
long double  test(const std::string  strtemp,const long double tC ) {
    //при моделировании использована  дельта времени(отрезок времени между двумя отсчётами). Но в модели представленной задачи и формул конечный результат отличается в зависимости от того,
    //какая дельта использована в расчётах.
    const long double delt = 0.01;
    const int size = 6;
    long double Vk, Vh, Vc, A, templd, Ctm=tC, tempC=tC, time = 0, T, Hm, Hv, C, Mm;
    int I, imv;
    int M[size], V[size];
    std::ifstream infile; 
    infile.open(strtemp);
    std::stringstream sstream;
    sstream << infile.rdbuf(); 
    //можно было считать  в цикле, более красиво,  но в условиях задачи такое исполнение сильно удобнее
    //обозначения переменных совпадают с указанными в условии
    sstream >> I >> M[0] >> M[1] >> M[2] >> M[3] >> M[4] >> M[5] >> V[0] >> V[1] >> V[2] >> V[3] >> V[4] >> V[5] >> T >> Hm >> Hv >> C;
    Vk = 0;
    Mm = M[0];
    A = Mm / I;
    while (tempC < T) {
        Vk = Vk + A * time;
        Vh = Mm * Hm + powl(Vk, 2) * Hv;
        Vc = C * (Ctm - tempC);
        tempC += (Vh + Vc) * time;
        time += delt;
        Mm = checkv(Vk, V, M, size);
        A = Mm / I;
        if ((Vh + Vc) * time <= 0) {
            std::cout << "Endless cycle\n";
            break;
        }
    }
    std::cout << "time to overheating = " << time << "  temp" << tempC << "\n";
    return time;
}